import itertools
import time
import random
import curses
import sys

bordure_sup = "┌───┬───┬───┐"
bordure_inf = "└───┴───┴───┘"
espacement = "        "

TEMPOS = [60, 40, 2010, 10, 8, 5, 3, 2, 1, 0.8, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0]

VITESSE = 2
TEMPO = 0.2
#piece = [centre, nw, ne, se, sw]
#1 = trou ; 0 = plein
PIECES = [("p1", [1, 1, 0, 0, 0]),\
          ("p2", [0, 1, 0, 0, 0]),\
          ("p3", [0, 1, 0, 1, 0]),\
          ("p4", [0, 1, 1, 0, 0])]

# probleme est une liste contenant successivement :
# le nombre de barques devant apparaitre
# le nombre de bateaux blancs devant apparaitre
# le nombre de bateaux rouges devant apparaitre
# le nombre de montagnes devant apparaitre
# le nombre d'iles devant apparaitre

PR1 = [0,5,0,0,0]
PR2 = [0,0,3,2,0]
PR3 = [3,0,0,0,2]
PR4 = [0,0,0,2,2]
PR5 = [0,0,3,1,0]
PR6 = [2,0,3,2,0]
PR7 = [0,1,0,2,0]
PR8 = [2,5,0,0,0]
PR9 = [0,1,3,0,0]
PR10 = [0,0,0,2,3]
PR11 = [2,0,3,0,0]
PR12 = [0,0,1,2,0]
PR13 = [0,5,1,0,0]
PR14 = [0,2,3,2,0]
PR15 = [0,0,2,2,3]
PR16 = [0,3,3,0,0]
PR17 = [0,3,0,0,0]
PR18 = [2,0,3,0,1]
PR19 = [1,0,1,2,3]
PR20 = [1,5,0,0,1]
PR21 = [0,1,3,1,2]
PR22 = [0,1,2,1,3]
PR23 = [0,1,1,0,1]
PR24 = [0,1,3,2,1]
PR25 = [0,3,3,0,1]
PR26 = [0,1,1,0,2]
PR27 = [0,1,2,0,0]
PR28 = [0,0,2,1,2]
PR29 = [0,4,0,0,2]
PR30 = [0,4,2,0,1]
PR31 = [0,1,2,2,2]
PR32 = [0,0,1,1,1]
PR33 = [1,0,0,2,3]
PR34 = [1,0,3,1,2]
PR35 = [0,0,3,1,1]
PR36 = [0,0,2,1,0]
PR37 = [3,1,1,1,0]
PR38 = [2,0,1,1,0]
PR39 = [0,4,0,1,2]
PR40 = [2,1,0,2,0]
PR41 = [2,0,3,1,1]
PR42 = [0,0,2,2,2]
PR43 = [2,0,1,2,0]
PR44 = [0,2,2,1,2]
PR45 = [0,1,2,0,2]
PR46 = [0,4,1,0,2]
PR47 = [1,1,2,0,3]
PR48 = [0,0,1,1,2]
PRS = [PR1, PR2, PR3, PR4, PR5, PR6, PR7, PR8, PR9,\
       PR10, PR11, PR12, PR13, PR14, PR15, PR16, PR17, PR18, PR19,\
       PR20, PR21, PR22, PR23, PR24, PR25, PR26, PR27, PR28, PR29,\
       PR30, PR31, PR32, PR33, PR34, PR35, PR36, PR37, PR38, PR39,\
       PR40, PR41, PR42, PR43, PR44, PR45, PR46, PR47, PR48]
RIEN = 0
BARQUE = 1
BATEAU_BLANC = 2
BATEAU_ROUGE = 3
MONTAGNE = 4
ILE = 5
PLATEAU = [[RIEN, BARQUE, BARQUE, MONTAGNE, BATEAU_BLANC],\
           [BATEAU_ROUGE, RIEN, MONTAGNE, BATEAU_BLANC, ILE],\
           [ILE, BATEAU_ROUGE, BATEAU_BLANC, BATEAU_BLANC, RIEN],\
           [BATEAU_BLANC, BARQUE, BATEAU_ROUGE, ILE, RIEN]]

S_RIEN= "   "
S_ILE = "\N{DESERT ISLAND}  "
S_BARQUE = "\N{ROWBOAT} "
S_VOILIER = "\N{SAILBOAT} " #bateau blanc
S_FERRY = "\N{FERRY}  "      #bateau rouge
S_MONTAGNE = "\N{Snow Capped Mountain}  "

S_PLEIN = "\N{Full Block}\N{Full Block}\N{Full Block}"
dessin = ["   ", S_BARQUE, S_VOILIER, S_FERRY, S_MONTAGNE,S_ILE]
dessin_piece = [S_PLEIN, S_RIEN]
noms_pieces = ["p1", "p2", "p3", "p4"]

ecran = None

def initCurses():
    global ecran
    ecran = curses.initscr()
    curses.noecho()
    curses.cbreak()
    ecran.keypad(True)

def endCurses():
    global ecran
    curses.nocbreak()
    ecran.keypad(False)
    curses.echo()
    curses.endwin()

def sep(symb1, symb2):
    if symb1 == S_PLEIN and symb2 == S_PLEIN:
        return "\N{Full Block}"
    else:
        return "│"
    
def getLignePiece(piece, ligne):
    #piece : la liste définissant la pièce
    if ligne == 1:
        symb1 = dessin_piece[piece[1]]
        symb2 = S_PLEIN
        symb3 = dessin_piece[piece[2]]
    elif ligne == 2:
        symb1 = S_PLEIN
        symb2 = dessin_piece[piece[0]]
        symb3 = S_PLEIN
    else:
        symb1 = dessin_piece[piece[4]]
        symb2 = S_PLEIN
        symb3 = dessin_piece[piece[3]]
        
    return "│" + symb1 + sep(symb1, symb2) + symb2 + sep(symb2, symb3) + symb3 + "│"
    
def getLigneCase(case, ligne):
    #case : le numéro de la case
    if ligne == 1:
        symb1 = dessin[PLATEAU[case][1]]
        symb2 = S_RIEN
        symb3 = dessin[PLATEAU[case][2]]
    elif ligne == 2:
        symb1 = S_RIEN
        symb2 = dessin[PLATEAU[case][0]]
        symb3 = S_RIEN
    else:
        symb1 = dessin[PLATEAU[case][4]]
        symb2 = S_RIEN
        symb3 = dessin[PLATEAU[case][3]]
        
    return "│" + symb1 + "│" + symb2 + "│" + symb3 + "│"

def get_ligne_case_vide():
    return "│" + S_RIEN + "│" + S_RIEN + "│" + S_RIEN + "│"

def getLigneCaseMasquee(case, ligne, piece):
    #case : le numéro de la case
    if ligne == 1:
        symb1 = dessin[PLATEAU[case][1]]
        symb2 = S_PLEIN
        symb3 = dessin[PLATEAU[case][2]]
        if piece[1] == 0:
            symb1 = S_PLEIN
        if piece[2] == 0:
            symb3 = S_PLEIN
    elif ligne == 2:
        symb1 = S_PLEIN
        symb2 = dessin[PLATEAU[case][0]]
        symb3 = S_PLEIN
        if piece[0] == 0:
            symb2 = S_PLEIN
    else:
        symb1 = dessin[PLATEAU[case][4]]
        symb2 = S_PLEIN
        symb3 = dessin[PLATEAU[case][3]]
        if piece[4] == 0:
            symb1 = S_PLEIN
        if piece[3] == 0:
            symb3 = S_PLEIN
        
    return "│" + symb1 + sep(symb1, symb2) + symb2 + sep(symb2, symb3) + symb3 + "│"

def afficher_plateau_pieces_partielles(probleme, solution, nb_tentatives):
    ligne_depart = curses.LINES - 20
    ligne_courante = afficher_probleme_curses(probleme, ecran, ligne_depart)
    for case in range(0,3,2):
        ecran.addstr(ligne_courante, 0, bordure_sup + espacement + bordure_sup)
        ligne_courante += 1
        if case == 0 or (case == 2 and len(solution) >= 3):
            representation_case_gauche_ligne_1 = getLigneCaseMasquee(case, 1, solution[case])
            representation_case_gauche_ligne_2 = getLigneCaseMasquee(case, 2, solution[case])
            representation_case_gauche_ligne_3 = getLigneCaseMasquee(case, 3, solution[case])
        else:
            representation_case_gauche_ligne_1 = get_ligne_case_vide()
            representation_case_gauche_ligne_2 = get_ligne_case_vide()
            representation_case_gauche_ligne_3 = get_ligne_case_vide()
        if (case == 0 and len(solution) >= 2) or (case == 2 and len(solution) == 4):
            representation_case_droite_ligne_1 = getLigneCaseMasquee(case+1, 1, solution[case+1])
            representation_case_droite_ligne_2 = getLigneCaseMasquee(case+1, 2, solution[case+1])
            representation_case_droite_ligne_3 = getLigneCaseMasquee(case+1, 3, solution[case+1])
        else:
            representation_case_droite_ligne_1 = get_ligne_case_vide()
            representation_case_droite_ligne_2 = get_ligne_case_vide()
            representation_case_droite_ligne_3 = get_ligne_case_vide()
            
        ecran.addstr(ligne_courante, 0, representation_case_gauche_ligne_1 + espacement + representation_case_droite_ligne_1)
        ligne_courante += 1
        ecran.addstr(ligne_courante, 0, representation_case_gauche_ligne_2 + espacement + representation_case_droite_ligne_2)
        ligne_courante += 1
        ecran.addstr(ligne_courante, 0, representation_case_gauche_ligne_3 + espacement + representation_case_droite_ligne_3)
        ligne_courante += 1
        ecran.addstr(ligne_courante, 0, bordure_inf + espacement + bordure_inf)
        ligne_courante += 1
        ligne_courante += 1
    ecran.addstr(ligne_courante, 0, "Nombre d'essais : " + str(nb_tentatives))
    ligne_courante += 1
    ecran.addstr(ligne_courante, 0, "Vitesse : " + str(VITESSE))
    ecran.refresh()

def afficher_plateau_pieces(solution):
    for case in range(0,3,2):
        print (bordure_sup, espacement, bordure_sup)
        print(getLigneCaseMasquee(case, 1, solution[case]), espacement, getLigneCaseMasquee(case+1, 1, solution[case+1]))
        print(getLigneCaseMasquee(case, 2, solution[case]), espacement, getLigneCaseMasquee(case+1, 2, solution[case+1]))
        print(getLigneCaseMasquee(case, 3, solution[case]), espacement, getLigneCaseMasquee(case+1, 3, solution[case+1]))
        print (bordure_inf, espacement, bordure_inf)
        print()
        print()

def afficher_plateau_vide():
    bordure = "-------------"
    espacement = "        "
    for case in range(0,3,2):
        print (bordure_sup, espacement, bordure_sup)
        print(getLigneCase(case, 1), espacement, getLigneCase(case+1, 1))
        print(getLigneCase(case, 2), espacement, getLigneCase(case+1, 2))
        print(getLigneCase(case, 3), espacement, getLigneCase(case+1, 3))
        print (bordure_inf, espacement, bordure_inf)
        print()
        print()


def afficher_liste_pieces():
    for nb_piece in range(4):
        print(bordure_sup, espacement, sep='', end='')
    print()
    for ligne in range(1,4):
        print(getLignePiece(PIECES[0][1], ligne), espacement,\
              getLignePiece(PIECES[1][1], ligne), espacement,\
              getLignePiece(PIECES[2][1], ligne) , espacement,\
              getLignePiece(PIECES[3][1], ligne), sep='')
    for nb_piece in range(4):
        print(bordure_inf, espacement, sep='', end='')
    print()

def plateau():
    case1ligne1 = "|" + BARQUE + "|   |" + BARQUE + "|"
    case1ligne2 = "|   |   |   |"
    case1ligne3 = "|" + VOILIER + "|   |" + MONTAGNE +"|"

    case2ligne1 = "|   |   |" + MONTAGNE + "|"
    case2ligne2 = "|   |" + FERRY + "|   |"
    case2ligne3 = "|" + ILE + "|   |" + VOILIER +"|"

    case3ligne1 = "|" + FERRY + "|   |" + VOILIER + "|"
    case3ligne2 = "|   |" + ILE + "|   |"
    case3ligne3 = "|   |   |" + VOILIER

    case4ligne1 = "|" + BARQUE + "|   |" + FERRY + "|"
    case4ligne2 = "|   |" + VOILIER + "|   |"
    case4ligne3 = "|   |   |" + ILE + "|"
    retour = bordure_sup + "\n" + case1ligne1 + "\n" + case1ligne2 + "\n" + case1ligne3 + "\n" + bordure_sup + "\n"
    retour += bordure + "\n" + case2ligne1 + "\n" + case2ligne2 + "\n" + case2ligne3 + "\n" + bordure + "\n"
    retour += bordure + "\n" + case3ligne1 + "\n" + case3ligne2 + "\n" + case3ligne3 + "\n" + bordure + "\n"
    retour += bordure_inf + "\n" + case4ligne1 + "\n" + case4ligne2 + "\n" + case4ligne3 + "\n" + bordure_inf
    return retour

def generer_rotations(piece):
    """
    Fonction générant toutes les rotations d'une pièce. Le centre (à l'indice 0) ne bouge pas
    """
    tete = piece[0]
    peripherie = piece[1:]
    retour = []
    for i in range(1,5):
        tour = peripherie[i:] + peripherie[:i]
        tour.insert(0, tete)
        retour.append(tour)
    return retour

def creer_table_rotations(pieces):
    """
    Création d'un dictionnaire associant à chaque pièce ses 4 rotations possibles.
    """
    retour = {}
    for couple in pieces:
        rotations = generer_rotations(couple[1])
        retour[couple[0]] = rotations
    return retour

def generer_config_rotations():
    """
    Création d'un tableau spécifiant toutes les possibilités de rotation des pièces
    à chaque position.
    """
    retour = []
    for i1 in range(4):
        for i2 in range(4):
            for i3 in range(4):
                for i4 in range(4):
                    retour.append([i1, i2, i3, i4])
    return retour

nb_tentatives = 0
def resoudre(pieces_disponibles, probleme, solution=[]):
    global VITESSE
    global nb_tentatives
    if len(pieces_disponibles) == 0:
        #print("plus de pièce à placer")
        if evaluer_final(probleme, solution):
            return solution
        else:
            return None
    for piece in pieces_disponibles:
        nom_piece = piece[0]
        #print("On essaie de placer la pièce",nom_piece,"en case",len(solution))
        pieces_restantes = pieces_disponibles[:]
        pieces_restantes.remove(piece)
        for position in table_rotations[nom_piece]:
            solution_suivante = solution + [position]
            nb_tentatives += 1
            afficher_plateau_pieces_partielles(probleme, solution_suivante, nb_tentatives)
            time.sleep(TEMPOS[VITESSE - 1])
            car = ecran.getch()
            if car == ord('+') and VITESSE < 20:
                VITESSE += 1
            elif car == ord('-') and VITESSE > 1:
                VITESSE -= 1
            elif car == ord('q') or car == ord('Q'):
                endCurses()
                print("Au revoir...")
                exit()
            if not evaluer(probleme, solution_suivante):
                #print("incorrect")
                continue
            #print("correct ; on continue")
            resultat = resoudre(pieces_restantes, probleme, solution_suivante)
            if resultat is not None:
                return resultat
    return None
    
def evaluer(probleme, solution):
    visu = [0, 0, 0, 0, 0, 0]
    for num_piece in range(len(solution)):
        for num_emplacement in range(5):
            visu[solution[num_piece][num_emplacement] *  PLATEAU[num_piece][num_emplacement]] += 1
    #print("visu actuelle : " + str(visu[1:]))
    if visu[BARQUE] > probleme[BARQUE-1]:
        #print("Trop de barques")
        return False
    if visu[BATEAU_BLANC] > probleme[BATEAU_BLANC-1]:
        #print("Trop de bateaux blancs")
        return False
    if visu[BATEAU_ROUGE] > probleme[BATEAU_ROUGE-1]:
        #print("Trop de bateaux rouges")
        return False
    if visu[MONTAGNE] > probleme[MONTAGNE-1]:
        #print("Trop de montagnes")
        return False
    if visu[ILE] > probleme[ILE-1]:
        #print("Trop d'îles")
        return False
    return True

def evaluer_final(probleme, solution):
    visu = [0, 0, 0, 0, 0, 0]
    for num_piece in range(len(solution)):
        for num_emplacement in range(5):
            visu[solution[num_piece][num_emplacement] *  PLATEAU[num_piece][num_emplacement]] += 1
    #print("visu actuelle : " + str(visu[1:]))
    if visu[BARQUE] != probleme[BARQUE-1]:
        #print("Pas assez de barques")
        return False
    if visu[BATEAU_BLANC] != probleme[BATEAU_BLANC-1]:
        #print("Pas assez de bateaux blancs")
        return False
    if visu[BATEAU_ROUGE] != probleme[BATEAU_ROUGE-1]:
        #print("Pas assez de bateaux rouges")
        return False
    if visu[MONTAGNE] != probleme[MONTAGNE-1]:
        #print("Pas assez de montagnes")
        return False
    if visu[ILE] != probleme[ILE-1]:
        #print("Pas assez d'îles")
        return False
    return True

def generer_position(table_rotations, permutation, config):
    position = []
    erreur = False
    for indice_piece in range(4):
        piece = permutation[indice_piece]
        numero_rotation = config[indice_piece]
        if piece == "p3" and numero_rotation > 1:
            # la pièce p3 possède une symétrie centrale ;
            # On considère donc ses positions 2 et 3 comme illégales
            raise Exception()
        else:
            position.append(table_rotations[piece][numero_rotation])
    return position

def verifier_correction(probleme, position):
    visu = []
    for i in range(4):
        visu.append([0] * 5)
    for case in range(4):
        for place in range(5):
            # Un trou étant représenté par un "1",  multiplier par un trou
            # préserve l'objet.
            # Un plein étant représenté par un "0", multipier par un plein
            # masque l'objet
            visu[case][place] = PLATEAU[case][place] * position[case][place]
    visu_globale = [objet for case in visu for objet in case]
    visu_resumee = [visu_globale.count(BARQUE),\
                    visu_globale.count(BATEAU_BLANC),\
                    visu_globale.count(BATEAU_ROUGE),\
                    visu_globale.count(MONTAGNE),\
                    visu_globale.count(ILE)]
    return visu_resumee == probleme

def afficher_probleme(probleme):
    for i in range(5):
        if probleme[i] > 0:
            print("  ",probleme[i], dessin[i+1])

def afficher_probleme_curses(probleme, ecran, ligne_depart):
    for i in range(5):
        if probleme[i] > 0:
            ecran.addstr(ligne_depart, 0, str(probleme[i]) + " " + dessin[i+1])
            ligne_depart += 1
    return ligne_depart

def jouer():
    print("PLATEAU : ")
    afficher_plateau_vide()
    print("PIECES : ")
    afficher_liste_pieces()
    probleme = random.choice(PRS)
    print("\n\nPositionnez les pièces sur les 4 emplacements pour faire apparaître exactement :")
    afficher_probleme(probleme)
    input()
    initCurses()
    ecran.nodelay(True)
    solution = resoudre(PIECES, probleme)
    ecran.nodelay(False)
    endCurses()
    afficher_plateau_pieces(solution)
    
def representer(piece):
  ligne1 = car(piece[1]) + "*" + car(piece[2])
  ligne2 = "*" + car(piece[0]) + "*"
  ligne3 = car(piece[4]) + "*" + car(piece[3])
  return [ligne1, ligne2, ligne3]

table_rotations = creer_table_rotations(PIECES)
config_rotations = generer_config_rotations()

if __name__ == "__main__":
    VITESSE = 8
    curses.initscr()
    nbLignes = curses.LINES
    curses.endwin()
    if nbLignes < 31:
        print("Terminal trop petit ; il manque",31-nbLignes,"lignes")
        exit()
    if len(sys.argv) > 2 or (len(sys.argv) == 2 and (sys.argv[1] == "-h" or sys.argv[1]== "--help")):
        print("usage : pirates [vitesse]")
        print("Où vitesse doit être un entier compris entre 1 (5 secondes de  pause) et 20 (pas de pause).")
        print("Pendant la recherche, il est possible de modifier la vitesse grâce aux touches '+' et '-'.")
        exit()
    elif len(sys.argv) == 2:
        #TEMPO = float(sys.argv[1])
        VITESSE = int(sys.argv[1])
        if VITESSE < 1 or VITESSE > 16:
            print("La vitesse doit être comprise entre 1 et 8")
            exit()
    try:
        jouer()
    except Exception as e:
        endCurses()
        print(str(e))

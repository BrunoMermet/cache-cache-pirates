# cache-cache pirates

## Présentation générale
Programme permettant de résoudre les problèmes du jeu de logique évolution *Cache-cache pirates*, jadis publié par *Smart Games*. Pour mieux comprendre le jeu, une version jouable en ligne est disponible ici :

https://www.smartgames.eu/fr/try-smartgames-online/jeux-pour-1-joueur/jungle-cache-cache.

## Quelques remarques
Les "graphismes" en mode texte ont été rajoutés à la ramasse juste avant une fête de la science alors que j'avais plein d'autres choses à faire. Du coup, sur ce plan là, le code est assez sale. Mais ça illustre ce qu'on peut faire avec un peu de *curses* et de l'UTF-8.
